<?php

use Gomzyakov\CS\Config;
use Gomzyakov\CS\Finder;

$finder = Finder::createWithRoutes([
    __DIR__ . '/wp-content/themes/test/',
]);

/**
 * @link https://mlocati.github.io/php-cs-fixer-configurator/
 */
$rules = [
    'binary_operator_spaces'                 => [
        'default'   => 'align_single_space_minimal',
        'operators' => [
            '='  => 'align_single_space',
            '=>' => 'align_single_space',
            '??' => 'single_space',
        ],
    ],
    'cast_spaces'                            => false,
    'declare_equal_normalize'                => ['space' => 'none'],
    'declare_parentheses'                    => true,
    'method_argument_space'                  => false,
    'multiline_whitespace_before_semicolons' => true,
    'no_blank_lines_after_phpdoc'            => false,
    'not_operator_with_successor_space'      => false,
    'phpdoc_align'                           => [
        'align' => 'vertical',
        'tags'  => [
            'param',
            'property',
            'property-read',
            'property-write',
            'return',
            'throws',
            'type',
            'var',
            'method',
        ],
    ],
    'phpdoc_separation'                      => true,
    'phpdoc_summary'                         => false,
    'phpdoc_no_package'                      => false,
    'no_trailing_whitespace_in_comment'      => true,
    'no_alternative_syntax'                  => false,
    'indentation_type'                       => true,
    'statement_indentation'                  => false,
];

return Config::createWithFinder($finder, $rules);
