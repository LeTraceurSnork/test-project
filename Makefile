#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/sh
DC_BIN = $(shell command -v docker-compose 2> /dev/null)
DC_RUN_ARGS = --rm --user "$(shell id -u):$(shell id -g)"

cwd = $(shell pwd)

.PHONY : help \
		 composer-install install test fix \
		 up down restart logs
.SILENT : help install up
.DEFAULT_GOAL : help

all: down build-image install up ## Starting the app

build-image: ## Building the app
	$(DC_BIN) build

help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

shell: ## Start shell into app container
	$(DC_BIN) run wordpress sh

composer-install: ## Install all backend dependencies
	$(DC_BIN) run $(DC_RUN_ARGS) --no-deps wordpress composer install

install: composer-install ## Install all app dependencies

test: ## Execute app tests
	$(DC_BIN) run wordpress composer test

fix: ## Execute source fixers
	$(DC_BIN) run --no-deps wordpress composer fix

up: ## Create and start containers
	$(DC_BIN) up --detach --scale queue=2
	@printf "\n   \e[30;42m %s \033[0m\n\n" 'Navigate your browser to: <http://127.0.0.1:1001>'

down: ## Stop and remove containers, networks, images, and volumes
	$(DC_BIN) down -t 5

restart: down up ## Restart all containers

pull: ## Pulling newer versions of used docker images
	$(DC_BIN) pull

logs: ## Show docker logs
	$(DC_BIN) logs --follow
